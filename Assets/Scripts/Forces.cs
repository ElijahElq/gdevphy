﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Forces : MonoBehaviour
{
    public Vector3 BirdObjectPos;
    public Vector3 BirdProjection;
    public Vector3 SlingPosition;

    public Vector3 Blackhole;

    public Vector3 GravityForce = new Vector3(0, -0.5f, 0);

    public GameObject BirdGameobject;

    Bird bird;

    // Start is called before the first frame update
    void Start()
    {
        bird = BirdGameobject.GetComponent<Bird>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ApplyLaunch(Vector3 force)
    {
        Vector3 applyForce = new Vector3(force.x / bird.mass, force.y / bird.mass, 0);

        bird.acceleration += applyForce;
    }

    public void ApplyAttract(GameObject BlackholePos, GameObject BirdPos)
    {
        Vector3 distance = BlackholePos.transform.position - BirdPos.transform.position;
        
        float attractMagnitude = distance.magnitude;
        BirdPos.transform.position = Vector3.MoveTowards(BirdPos.transform.position, BlackholePos.transform.position, attractMagnitude * Time.deltaTime);
    }

    public void CalculateTrajectory()
    {

    }

    public void CalculateAttract()
    {

    }
}
