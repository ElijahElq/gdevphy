﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    public Vector3 acceleration = new Vector3(0, 0, 0);
    public Vector3 velocity = new Vector3(0, 0, 0);
    public float mass = 5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        velocity += acceleration;
        transform.position += velocity * Time.deltaTime;
        acceleration *= 0;
    }
}
